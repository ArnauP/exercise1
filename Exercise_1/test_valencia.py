import valencia

def test_is_valid():
    assert valencia.is_valid([1, 2, 3]) is True
    assert valencia.is_valid(["123", "2", "53"]) is True
    assert valencia.is_valid(["123", "2", "-32"]) is False
    assert valencia.is_valid(["123", "a", "32"]) is False

def test_valencia():
    assert valencia.valencia('15741') == 0
    assert valencia.valencia('2222') == 0
    assert valencia.valencia('6') == 6
    assert valencia.valencia('951237') == 1
