import sys

def is_valid(sequence):
    """
    Given a list, returns True only when the sequence is made out of natural numbers.
    """
    try:
        for number in sequence:
            if int(number) < 0:
                raise Exception
    except Exception:
        return False
    else:
        return True

def valencia(number):
    """
    Given a string, returns the absolute value of the substraction between the sum of the digits that are in odd positions and the sum of the digits in even positions.
    """
    if not len(number) > 1:
        # In case of only one digit the same digit is returned.
        return int(number)

    # Search by position all digits of the given number.
    even = 0
    odd = 0
    for pos in range(len(number)):
        if int(pos) % 2 == 0:
            # Sum the numbers in the even positions.
            even += int(number[pos])
        else:
            # Sum the numbers in the odd positions.
            odd += int(number[pos])
    # Return the absolute result of the substraction.
    return abs(even - odd)


if __name__ == "__main__":
    # Sequence validation
    if not len(sys.argv) > 1:
        print("ERROR: Null argument. Write a sequence of natural numbers")
        sys.exit()
    sys.argv.remove(sys.argv[0])
    if not is_valid(sys.argv):
        print("ERROR: Invalid sequence. Only natural numbers allowed")
        sys.exit()

    # Expected output
    notBalanced = []
    for number in sys.argv:
        if valencia(number) == 0:
            print("First balanced number found in the sequence: " + str(number))
            break
        else:
            notBalanced.append(int(number))
            if len(notBalanced) == len(sys.argv):
                print("Could not find any balanced number.")
                print("Greatest valencia number found: " + str(max(notBalanced)))
