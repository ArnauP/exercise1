# Exercise 1

This is the first exercise of a three part test. It contains a python program that, given a sequence of natural numbers, is designed to print the first balanced number its given, in the absence of it, it prints the greates *valencia* of the numbers in the sequence.

### Requirements

To properly execute the program it is required to have installed:

* Python 3.6 or above.


### Usage

Note that our working direcotry shold be Exercise_1/, or specifiy in the command argument the path */Exercise_1/valencia.py*. Some basic usage commands would be:

**Running the program in Exercise_1/ as working directory:**
```
python3 valencia.py 2317 5 15741 22
```

**Expected result:**
```
First balanced number found in the sequence: 15741
```


**Running the program specifying the path:**
```
python3 Exercise_1/valencia.py 456 88 1 98
```

**Expected result:**
```
First balanced number found in the sequence: 88
```


### Testing

In order to ensure that the program is reliable I have developed some tests. To execute the tests it is required to have installed:

* Pytest for python3

**Running the tests:**
```
python3 -m pytest
```